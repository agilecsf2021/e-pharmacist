import {showAlert} from './alert.js'
//logging out
const logout = async () => {
    try{
        const res = await axios({
            method: "GET",
            url: "http://localhost:4001/api/v1/users/logout",

        })
        if (res.data.status === 'success') {
            location.reload(true)
        }
    }catch(err){
        showAlert('error', 'Error logging out! try again.')
    }
}

var obj
if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))
}else {
    obj = JSON.parse('{}')
}

var el = document.querySelector('.nav.nav--user')
if(obj._id) {
    el.innerHTML =
    '<a id= "logout" class="nav__el">Log out</a> <a href="/me" class="nav__el"><img src="../img/users/'+
    obj.photo+
    ' " alt="Phot0 of ${user.name}" class="nav__user-img" /><span>' +
    obj.name+
    '</span></a>'
    var doc = document.querySelector('#logout')

    doc.addEventListener('click', (e) => logout())
}else {
    el.innerHTML = 
    '<a class="nav__el nav__el--cta" href="/login"> Log in </a> <a class="nav__el nav__el--cta" href="/signup" > Sign up </a>'
}

const allNews = async () => {
    try{
        const res = await axios({
            method:'GET',
            url:'http://localhost:4001/api/v1/news',
        })
        displayNews(res.data)

    }
    catch(err){
        console.log(err)
    }
}
allNews()

const displayNews = (news) => {
    var arr = news.data.news1
    console.log(arr)
    for(let i=0; i<arr.length; i++){
        var card = document.querySelector('.card').cloneNode(true)
        var e11 = card.querySelector('.card__picture-img')
        var e12 = card.querySelector('.card__sub-heading')
        var e13 = card.querySelector('.card__text')
        var e14 = card.querySelector('.publishedAt')
        var e15 = card.querySelector('.publishedBy')
        const element = arr[i]
        e11.innerHTML = '' + element.imageCover
        e12.innerHTML = '' + element.title
        e13.innerHTML = '' + element.description
        var d = new Date(element.publishedAt)
        e14.innerHTML = 
            ''+
            d.toLocaleString('en-US',{
                month:'long',
                year:'numeric',
            })
        e15.innerHTML = '' + element.user.name
        var card2 = document.querySelector('.card')
        card2.insertAdjacentElement('afterend', card)

    }
    document.querySelector('.card').remove()
}

